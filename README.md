
Golang 的并发模型非常强大，称为 CSP(通信顺序进程)，它将一个问题分解成更小的顺序进程，然后调度这些进程的实例(称为 Goroutine)。这些进程通过 channel 传递信息实现通信。

> Don't communicate by sharing memory; share memory by communicating.
> 不要通过共享内存来通信; 而要通过通信来共享内存


### 参考目录

[1 断言](http://c.biancheng.net/golang/intro/)  这里面很多章节收费的，空余时间多可以买来看

[2 worker pool 2](https://mp.weixin.qq.com/s?__biz=MzI2MDA1MTcxMg==&mid=2648468414&idx=1&sn=8efed31baa411f2e63e4fe043f207c41&chksm=f2474dd1c530c4c71f94dda44bb97201164df4a9730a5045534cdf354b54b096321e1f1b91a7&cur_album_id=1764229225982509062&scene=189#wechat_redirect)

### 社区，学习

[Go技术论坛](https://learnku.com/go)

[通过测试学习GO变成](https://learnku.com/docs/learn-go-with-tests/helloworld/6080)