
### 编写测试

- 编写测试和函数很类似，其中有一些规则
- 程序需要编写在 xxx_test.go 文件中
- 测试函数的命名必须以单词 Test 开始
- 测试函数只接受一个参数 t *testing.T

```
go test path/to/file/filename_test.go
```