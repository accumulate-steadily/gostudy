package hello

import "testing"

func Hello() string {
	return "Hello World"
}

func TestHello(t *testing.T) {
	got := Hello()
	want := "Hello World"
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}