package tests

import (
	"fmt"
	"testing"
)

func TestInterface(t *testing.T) {
	var x interface{}
	x = 10
	value, ok := x.(int)
	fmt.Println(value, ok)

	text := ""
	if len(text) == 0{
		fmt.Println("空字符串")
	}
}
