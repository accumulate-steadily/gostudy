package tests

import (
	"fmt"
	"math/rand"
	"study/workerpool"
	"testing"
	"time"
)

func WorkPoolTest(t *testing.T)  {
	// 创建了 100 个任务并且使用 6 个并发处理这些任务。
	var allTask []*workerpool.Task

	//for i := 1; i <= 100; i++ {
	//	task := workerpool.NewTask(func(data interface{}) error {
	//		// 断言
	//		taskID, ok := data.(int)
	//		time.Sleep(100 * time.Millisecond)
	//		if ok {
	//
	//		}
	//		fmt.Printf("Task %d processed\n", taskID)
	//		return nil
	//	}, i)
	//	allTask = append(allTask, task)
	//}

	pool := workerpool.NewPool(allTask, 5)
	go func() {
		for {
			taskID := rand.Intn(100) + 20

			if taskID%7 == 0 {
				pool.Stop()
			}

			time.Sleep(time.Duration(rand.Intn(5)) * time.Second)
			task := workerpool.NewTask(func(data interface{}) error {
				taskID := data.(int)
				time.Sleep(100 * time.Millisecond)
				fmt.Printf("Task %d processed\n", taskID)
				return nil
			}, taskID)
			pool.AddTask(task)
		}
	}()
	pool.RunBackground()
}
