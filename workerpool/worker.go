package workerpool

import (
	"fmt"
	"sync"
)

// v0.0.1 扩展解决方案，以便 worker 可以在后台等待我们投递新的任务并处理
type Worker struct {
	ID       int
	taskChan chan *Task
	quit     chan bool
}

// NewWorker return new instance of worker
func NewWorker(channel chan *Task, ID int) *Worker {
	return &Worker{
		ID:       ID,
		taskChan: channel,
		quit:     make(chan bool),
	}
}

// Start starts the worker
func (wr *Worker) Start(wg *sync.WaitGroup) {
	fmt.Println("Starting worker %d\n", wr.ID)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for task := range wr.taskChan {
			process(wr.ID, task)
		}
	}()
}

func (wr *Worker) StartBackgroud()  {
	fmt.Printf("Starting worker %d\n", wr.ID)
	for  {
		select {
		case task := <-wr.taskChan:
			process(wr.ID, task)
		case <- wr.quit:
			return
		}
	}
}

// Stop() 方法用于停止 worker
func (wr *Worker) Stop()  {
	fmt.Printf("Closing worker %d\n", wr.ID)
	go func() {
		wr.quit <- true
	}()
}