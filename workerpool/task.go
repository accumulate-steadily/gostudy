package workerpool

import "fmt"

type Task struct {
	Err error
	Data interface{} // 错误，数据类型 任意类型
	f func(interface{}) error // 函数 参数任意类型 返回错误
}

func NewTask(f func(interface{}) error, data interface{}) *Task {
	return &Task{f: f, Data:data}
}

func process(workerId int, task *Task)  {
	fmt.Printf("Worker %d processes task %v\n", workerId, task.Data)
	task.Err = task.f(task.Data)
}